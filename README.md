# BANCO

![Modelo](Modelo.jpeg)

# Comandos úteis

* ``@can('update-post', $post)`` //Filtro para apenas o usuarios logado ver seu propio post no front

* ``Gate::define('update-post',function($user,$post){
            return $user->id == $post->user_id;
        });`` //Método de ACL para apenas o usuario logado ver o seu propio post

* ``public function user(){
        return $this->belongsTo (User::class);
    }`` // Retorna os dados da model User de muitos para um quando a user for chamada dentro da model Post

* ``$this->authorize('update-post',$post);`` //Bloquear accesso em paginas não permitidas

* ``if (Gate::denies('update-post',$post)) 
            abort(403,'Não autorizado');`` //forma alternativa de bloquar acesso com mensagem de erro personalizada

# Informações

``@can('update-post',$post)`` serve para ocultar do usuario que não tem premissão da view, ou não tenha sido ele quem criou.