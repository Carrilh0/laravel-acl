@extends('layouts.app')

@section('content')
<div class="container">
    @forelse($posts as $post)
        @can('view_post',$post)
            <h1>{{$post->title}}</h1>
            <p>{{$post->description}}</p>
            
            <p>Autor: {{$post->user->name}}
            <br>
            @can('edit_post',$post)
            <a href="{{url("/post/$post->id/update")}}">Editar</a>
            @endcan
        <hr>

        @else
        <p>Sem permissão para ver os posts</p>

        @endcan
            
        
    @empty
        <p>Nenhum post encontrado</p>
    @endforelse
</div>
@endsection
