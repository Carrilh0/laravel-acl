<?php

namespace App\Http\Controllers;
use App\Post;
use Gate;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Post $post)
    {
        $posts = $post->all();    //retorna todos os posts
        //$posts = $post->where('user_id', auth()->user()->id)->get();    //retorna apenas os posts do usuarios logado

        return view('home',compact('posts'));
    }

    public function update($idPost){    //pega o id da view

        $post = Post::find($idPost);    //procura o post de acordo com a view q foi clicada
        
        //$this->authorize('update-post',$post); //Bloquear accesso

        if (Gate::denies('update',$post))  //forma alternativa de bloquar acesso com mensagem de erro personalizada
            abort(403,'Não autorizado');
        


        return view('update-post',compact('post'));
    }

    public function rolesPermissions(){
        $nameUser = auth()->user()->name;   //retorna o nome do usuario logado
        echo("<h1>$nameUser</h1>");         //imprimo o nome do usuario logado

        foreach(auth()->user()->roles as $role){
            echo "<b><hr>".$role->name."</b><br>";  //mostra o nome da role do usuario logado

            $permissions = $role->permissions;
            foreach($permissions as $permission){
                echo $permission->name."<br>";      //mostra as permissões da role do usuario logado
            }
        }
    }
}
